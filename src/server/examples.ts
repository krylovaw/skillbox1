//concat Hello World
function concatTS(str1: string, str2: string) {
    return str1 + str2;
}

let concatStr = concatTS('Hello ', 'World');

// interface
type MyArr = [string, string | number, number?];
interface IMyHomeTask {
    howIDoIt: string,
    someArray: MyArr,
    withData: [{
        howIDoIt: string,
        someArray: MyArr
    }]
}

const MyHometask: IMyHomeTask = {
    howIDoIt: "I Do It Wel",
    someArray: ["string one", "string two", 42],
    withData: [{ howIDoIt: "I Do It Wel", someArray: ["string one", 23] }],
}

//generics
interface MyArray<T> {
    [N: number]: T;   
    map<U>(fn: (el: T, index: number, arr: MyArray<T>) => U) : MyArray<U>;
    reduce<U>(fn: (accumulator: number, val: number) => U, initValue: number) : MyArray<U>;
  }

//добавьте типизацию для метода reduce
const myArray: Array<number> = [1,2,3];
[1,2,3].map((f) => f + 1)
myArray.map((f) => f + 1);
const initialValue: number = 0;
[1,2,3].reduce((accum, value) => accum + value, initialValue);

let newReduceVar = myArray.reduce((accum, value) => accum + value, initialValue);

interface IHomeTask {
    data: string;
    numbericData: number;
    date: Date;
    externalData: {
        basis: number;
        value: string;
    }
}

const homeTask: MyPartial<IHomeTask> = {
    externalData: {
        value: 'win'
    }
}

//все ключи не обязательны, поэтому запись выше возможна
type MyPartial<T> = {
    [N in keyof T]?: T[N] extends object ? MyPartial<T[N]> : T[N];
}


type MyPick<T, K extends keyof T> = {
    [N in K]: T[N];
}

type picked = MyPick<IHomeTask, 'data'>;