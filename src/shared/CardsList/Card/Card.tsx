import React from 'react';
import styles from './card.css';
import { ProfileLine } from './ProfileLine';
import { KarmaCounter } from './KarmaCounter';
import { CommentsButton } from './CommentsButton';
import { Actions } from './Actions';

export function Card() {
  return (
    <li className={styles.card}>
        <div className={styles.textContent}>
          <ProfileLine/>
          <h2 className={styles.title}>
            <a href='#post-url' className={styles.postLink}>
              New post about tehnic and other interesting things and many many many 
              good work and and .....New post about tehnic and other interesting things and many many many 
              good work and and ....
            </a>
          </h2>
        </div>
        <div className={styles.preview}>
          <img
            className={styles.previewImg}
            src='https://stringfixer.com/files/142083667.jpg'
            />
        </div>
        <div className={styles.menu}>
          <button className={styles.menuButton}>
          <svg width="5" height="20" viewBox="0 0 5 20" fill="none" xmlns="http://www.w3.org/2000/svg">
          <circle cx="2.5" cy="2.5" r="2.5" fill="#D9D9D9"/>
          <circle cx="2.5" cy="10" r="2.5" fill="#D9D9D9"/>
          <circle cx="2.5" cy="17.5" r="2.5" fill="#D9D9D9"/>
          </svg>
          </button>
        </div>
        <div className={styles.controls}>
          <KarmaCounter/>
          <CommentsButton/>
          <Actions/>          
        </div>
    </li>
  );
}
