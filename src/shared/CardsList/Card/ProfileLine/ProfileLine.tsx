import React from 'react';
import styles from './profileline.css';

export function ProfileLine() {
  return (
    <div className={styles.metaData}>
      <div className={styles.userLink}>
        <img 
        className={styles.avatar}
        src='https://i.pinimg.com/originals/b8/8b/4a/b88b4a8aebc154be4d20293912f15732.jpg'
        alt='avatar'/>
        <a href='#user-url' className={styles.username}>Vera Krylova</a>
      </div>
      <span className={styles.createdAt}>
        <span className={styles.publishedLabel}>опубликовано </span>  4 hours ago
      </span>
    </div>
  );
}
