import  * as React from 'react';
import { hot } from 'react-hot-loader/root';
import styles from './starWarsNameClass.css';
import {uniqueNamesGenerator, starWars } from 'unique-names-generator';

export default class StarWarsNameClass extends React.PureComponent {
    public render() {
        return (
            <section className={styles.container}>
                <span className={styles.name}>{this.randomName()}</span>
                <div className={styles.gap}/>
                <button className={styles.button}>Мне нужно имя!</button>
            </section>
        );
    }

    private randomName(): string {
        return uniqueNamesGenerator({dictionaries: [starWars], length: 1});
    }
}

 